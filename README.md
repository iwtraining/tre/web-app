# Mesario Online: como rodar

Esse projeto foi feito pra mostrar as zonas e fazer cadastros de mésarios



## Available Scripts

In the project directory, you can run:

### `yarn install`

Para instalar as dependencias

### `yarn start`

Para rodar a aplicação no modo "desenvolvimento.\
Acesse [http://localhost:3000](http://localhost:3000) para ver o resultado no navegador.

### `npx pwa-asset-generator public/img/logo.png ./public/assets -i ./public/index.html -m ./public/manifest.json`

Para atualizar o splash screen.


### `yarn build`

Para criar um executável da aplicação, lembre-se de apontar o DNS/Virtual host para `/build/index.html`

## Saiba mais

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
