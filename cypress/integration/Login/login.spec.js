describe('Login', () => {
    beforeEach(() => {
        cy.viewport('iphone-7');

        cy.visit(Cypress.env('APP_URL'));
    });

    function sendForm(email, password) {
        cy.get('[data-cy="email"]').type(email);

        cy.get('[data-cy="password"]').type(password);

        cy.get('[data-cy="btn-ok"]').click();
    }

    it('Email incorreto', () => {
        sendForm('naoexiste@email.com', '123123123');

        cy.contains('Email incorreto');
    });

    it('Senha incorreta', () => {
        sendForm('zzzzzz@email.com', '123123123');

        cy.contains('Senha inválida');
    });

    it ('Login correto', () => {
        sendForm('zzzzzz@email.com', '12345678');

        // cy.contains('Bem vindo');

        cy.get('[data-cy="page-title"]').text().should('eq', 'Bem vindo');


        cy.get('[data-cy="page-title"]').then(element => {
            cy.expect(element.text()).to.equal('Bem vindo');
        });

        // 

        // cy.wait(2000);
    });
});