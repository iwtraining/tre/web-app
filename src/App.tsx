import {BrowserRouter} from "react-router-dom";
import Footer from "./components/Footer";
import Navbar from "./components/Navbar";
import Routes from "./config/Routes";
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { userIsLogged } from "./security/Auth";
import Login from "./pages/Login";
import { useState } from "react";
import i18n from "i18next";
import { useTranslation, initReactI18next } from "react-i18next";
import { EN } from "./translation/en";
import { PTBR } from "./translation/ptbr";
import { FR } from "./translation/fr";


console.log(EN);

i18n
  .use(initReactI18next) 
  .init({
    resources: {
      en: EN,
      ptbr: PTBR,
      fr: FR
    },
    lng: "ptbr", 
    fallbackLng: "ptbr",
    interpolation: {
      escapeValue: false 
    }
  });

const myTheme = createTheme({
  palette: {
    primary: {
      main: '#1976d2'
    },
    secondary: {
      main: '#9c27b0',
    },
  },
});

function App() {
  const [logged, setLogged] = useState<any>(userIsLogged());

  const {t} = useTranslation();

  const Content = () => {
    if (logged) {
      return (
        <>
          <Navbar handleLogged={() => setLogged(false)}/>
          <Routes/>
        </>
      );
    }

    return (<Login handleLogged={() => setLogged(true)}/>);
  }

  return (
    <>
      <BrowserRouter>
        <ThemeProvider theme={myTheme}>
          <Content/>

          <Footer/>
        </ThemeProvider>
      </BrowserRouter>
    </>
  );
}

export default App;