import { Divider } from "@mui/material";
import "./styles.scss";

export default function Footer () {
    let date = new Date();

    return (
        <div className="footer">
            <Divider/>

            <p><strong>WebApp Mesário</strong> - {date.getFullYear()}</p>            

            <p>Todos os direitos reservados</p>
        </div>
    );
}