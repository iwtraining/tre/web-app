import * as React from 'react';
import {
    Menu, 
    MenuItem, 
    IconButton, 
    Typography, 
    Toolbar, 
    AppBar, 
    Drawer,
    Box, 
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    Divider
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import InboxIcon from '@mui/icons-material/Inbox';
import MailIcon from '@mui/icons-material/Mail';
import AccountCircle from '@mui/icons-material/AccountCircle';
import { Link } from 'react-router-dom';
import "./styles.scss";
import { CarRepair } from '@mui/icons-material';
import { logout } from '../../security/Auth';

export default function Navbar({handleLogged}: any) {
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const [menuDrawer, setMenuDrawer] = React.useState<boolean>(false);
    const [title, setTitle] = React.useState<string>('Bem vindo');

    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleOption = (newTitle: string) => {
        setTitle(newTitle);

        setMenuDrawer(false);
    };

    return (
        <>
            <AppBar position="static" className="navbar">
                <Toolbar>
                <IconButton
                    size="large"
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    sx={{ mr: 2 }}
                    onClick={() => setMenuDrawer(true)}
                >
                    <MenuIcon />
                </IconButton>
                <Typography data-cy="page-title" variant="h6" component="div" sx={{ flexGrow: 1 }}>
                    {title}
                </Typography>
                
                    <div>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleMenu}
                            color="inherit"
                        >
                            <AccountCircle />
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorEl}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >
                            <MenuItem onClick={handleClose}>
                                <Link to="/meu-perfil">Minha Conta</Link>
                            </MenuItem>
                            <MenuItem onClick={() => {logout(); handleLogged()}}>Sair</MenuItem>
                        </Menu>
                    </div>
                </Toolbar>
            </AppBar>

            <Drawer
                anchor="left"
                open={menuDrawer}
                onClose={() => setMenuDrawer(false)}
            >
                <Box role="presentation" sx={{width: 250}}>
                    <List>
                        <ListItem button component={Link} to="/veiculos" onClick={() => handleOption('Veiculos')}>
                            <ListItemIcon>
                                <CarRepair />
                            </ListItemIcon>
                            <ListItemText primary={"Veiculos"} />
                        </ListItem>
                        <ListItem button component={Link} to="/zonas" onClick={() => handleOption('Zonas')}>
                            <ListItemIcon>
                                <InboxIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Zonas"} />
                        </ListItem>

                        <ListItem button onClick={() => handleOption('Relatórios')}>
                            <ListItemIcon>
                                <MailIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Relatórios"} />
                        </ListItem>

                        <ListItem button component={Link} to="/usuarios/novo" onClick={() => handleOption('Cadastrar novo usuário')}>
                            <ListItemIcon>
                                <MailIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Usuários"} />                            
                        </ListItem>   

                        <ListItem button component={Link} to="/perguntas" onClick={() => handleOption('Perguntas frequentes')}>
                            <ListItemIcon>
                                <MailIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Perguntas frequentes"} />                            
                        </ListItem>                     

                        <ListItem button component={Link} to="/users" onClick={() => handleOption('Lista de usuários')}>
                            <ListItemIcon>
                                <MailIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Lista de Usuários"} />                            
                        </ListItem>                        

                        <Divider/>

                        <ListItem button onClick={() => {logout(); handleLogged()}}>
                            <ListItemIcon>
                                <InboxIcon />
                            </ListItemIcon>
                            <ListItemText primary={"Sair"} />
                        </ListItem>
                    </List>
                </Box>
            </Drawer>
        </>
    );
}