import {Route, Switch} from "react-router-dom";
import Example from "../../pages/Example";
import AddUser from '../../pages/User/Add';
import Home from "../../pages/Home";
import Login from "../../pages/Login";
import Profile from "../../pages/User/Profile";
import ListZones from "../../pages/Zone/List";
import ListUsers from "../../pages/User/List";
import Zone from "../../pages/Zone/Show";
import PaginaErro from '../../pages/PaginaErro';
import Faq from "../../pages/PerguntasFrequentes";
import ListVehicles from "../../pages/Vehicle/List";
import Camera from "../../pages/Camera";

export default function Routes () {
    return (
        <Switch>
            <Route path="/" exact component={Home}/>
            <Route path="/usuarios/novo" exact component={AddUser}/>
            <Route path="/exemplo" exact component={Example}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/meu-perfil" exact component={Profile}/>
            <Route path="/zonas" exact component={ListZones}/>
            <Route path="/perguntas" exact component={Faq}/>
            <Route path="/users" exact component={ListUsers}/>
            <Route path="/zonas/:id" exact component={Zone}/>
            <Route path="/veiculos" exact component={ListVehicles}/>
            <Route path="/camera" component={Camera}/>
            <Route path="/*" component={PaginaErro}/>                          
        </Switch>
    );
}