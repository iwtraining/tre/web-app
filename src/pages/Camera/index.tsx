import ReactCamera from 'react-html5-camera-photo';

export default function Camera() {
    return (
        <div>
            <ReactCamera
                onTakePhoto={(url) => console.log(url)}
            />
        </div>
    )
}