import * as React from 'react';
import {Button, TextField} from "@mui/material";

export default function Example() {
    return (
        <>
            <h1>
                Pagina de exemplo
            </h1>
            
            <div>
                <TextField label="Email" />
            </div>
            
            <Button color="primary" variant="contained">
                Teste
            </Button>

            <Button color="secondary" variant="contained">
                Teste
            </Button>

            <Button color="success" variant="contained">
                Teste
            </Button>

            <Button color="error" variant="contained">
                Teste
            </Button>
        </>
    )
}