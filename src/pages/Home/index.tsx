import * as React from 'react';

import {Button, MobileStepper, Paper, Typography, Box} from "@mui/material";
import { useTheme } from '@mui/material/styles';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';

const steps = [
    {
      label: 'Cadastro de mesário',
      description: `A Justiça Eleitoral, Bem vindo nos anos em que se realizam Eleições Oficiais, necessita da colaboração de cidadãos previamente selecionados, dentre eleitores maiores de 18 anos, que são convocados para atuar como seus auxiliares nessa nobre missão de garantir a todo o povo brasileiro o exercício livre e consciente do voto, visando a escolha de seus representantes políticos. Dessa forma, a participação do eleitor convocado pela Justiça Eleitoral como MESÁRIO constitui um ato de civismo de extrema importância e fundamental para a consolidação da democracia em nosso país.`,
    },
    {
      label: 'Como funciona',
      description:
        'A mencionada ferramenta permite que os colaboradores (mesários, auxiliares de eleição etc.) sejam convocados por meio eletrônico, mediante o envio de carta convocatória ao correio eletrônico (e-mail) ou aplicativo de mensagem instantânea, como o Whatsapp, informados pelo eleitor após prévio cadastro, autorizando tal procedimento.',
    },
    {
      label: 'Vantagens',
      description: `Dentre as vantagens proporcionadas pelo mencionado sistema, ressalta-se o aumento da agilidade na convocação pelos Juízos Eleitorais, proporcionando ao eleitor maior comodidade e segurança, bem como a redução de resíduos ao meio ambiente e de custos relacionados ao envio de cartas convocatórias por meio dos Correios, contribuindo, assim, com a política de desenvolvimento sustentável da Justiça Eleitoral.`,
    },
  ];

export default function Home() {
    const theme = useTheme();
    const [activeStep, setActiveStep] = React.useState(0);
    const maxSteps = steps.length;

    const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    return (
        <Box sx={{ maxWidth: '100%', flexGrow: 1 }}>
            <Paper
            square
            elevation={0}
            sx={{
                display: 'flex',
                alignItems: 'center',
                height: 50,
                pl: 2,
                bgcolor: 'background.default',
            }}
            >
            <Typography>{steps[activeStep].label}</Typography>
            </Paper>
            <Box sx={{ height: 255, maxWidth: 400, width: '100%', p: 2 }}>
            {steps[activeStep].description}
            </Box>
            <MobileStepper
            variant="text"
            steps={maxSteps}
            position="static"
            activeStep={activeStep}
            nextButton={
                <Button
                size="small"
                onClick={handleNext}
                disabled={activeStep === maxSteps - 1}
                >
                Próximo
                {theme.direction === 'rtl' ? (
                    <KeyboardArrowLeft />
                ) : (
                    <KeyboardArrowRight />
                )}
                </Button>
            }
            backButton={
                <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
                {theme.direction === 'rtl' ? (
                    <KeyboardArrowRight />
                ) : (
                    <KeyboardArrowLeft />
                )}
                Anterior
                </Button>
            }
            />
        </Box>
        )
}