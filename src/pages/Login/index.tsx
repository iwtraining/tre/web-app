import {Button, TextField, Snackbar} from "@mui/material";
import { useState } from "react";
import { API_URL } from "../../config/Api";
import { login } from "../../security/Auth";
import {useTranslation} from "react-i18next";
import "./styles.scss";

export default function Login(props: any) {
    const [error, setError] = useState<string>('');
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');

    const {t} = useTranslation();

    const onFinish = (event: any) => {
        event.preventDefault();
        
        let request = {
            email: email,
            password: password,
        };

        fetch(API_URL+'/login', {
            method: 'POST',
            body: JSON.stringify(request),
            headers: {"Content-Type": 'application/json'}
        })
            .then(response => response.json())
            .then(response => {
                if (typeof(response) === 'string') {
                    setError(response);
                    return;
                }

                if (response.accessToken) {
                    login(response);
                    props.handleLogged();
                }
            })
            .catch(error => alert('erro de conexao'));
    };
    
    return (
        <section className="page-login">
            <h1>Login</h1>

            <Snackbar
                anchorOrigin={{vertical: 'top', horizontal: 'right' }}
                open={error?true:false}
                onClose={() => setError('')}
                message={t(error)}
            />

            <form onSubmit={onFinish}>
                <TextField
                    data-cy="email"
                    required
                    id="outlined-required"
                    label="Email"
                    name="email"
                    onChange={(event) => setEmail(event.target.value)}
                    fullWidth
                />

                <TextField
                    data-cy="password"
                    id="outlined-password-input"
                    label={t('password')}
                    type="password"
                    name="password"
                    required
                    autoComplete="current-password"
                    onChange={(event) => setPassword(event.target.value)}
                    fullWidth
                />

                <Button data-cy="btn-ok" fullWidth type="submit" variant="contained">Entrar</Button>
            </form>        
        </section>
    );
}