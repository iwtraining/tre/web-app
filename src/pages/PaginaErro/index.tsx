import { Link } from 'react-router-dom';
import { Button } from '@mui/material';
import "./styles.scss";

export default function PaginaErro() {    
    return (
        <div className="page-error-404">
            <div> 
                <h1>Página não encontrada</h1>
                <img alt="Nao encontrado" src={process.env.PUBLIC_URL + '/assets/images/pagina_erro.png'} width="500" height="300"/>             
            </div>

            <Button variant="outlined" color="secondary">      
                <Link to="/">
                    Voltar para o início
                </Link>
            </Button>
        </div>
    );
}