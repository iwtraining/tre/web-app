import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { useEffect, useState } from 'react';

export default function PerguntasFrequentes() {
    interface Faq {
        id: string,
        question: string,
        answer: string
    };

    const [faqs, setFaqs] = useState<Faq[]>([]);
    
    useEffect(() => {
        let url:string = process.env.REACT_APP_API_URL + '/faq/';
        
        fetch(url)
          .then(response => response.json())
          .then(faqs => {
            setFaqs(faqs);
          })
          .catch(error => {
            alert("Não foi possível ler o banco de dados!");
          });
   
    }, []);

  return (
    <div>
        {faqs.map((faq) => {
            return (
                <Accordion>
      
                <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
                >
                <Typography>{faq.question}</Typography>
                </AccordionSummary>
                <AccordionDetails>
                <Typography>
                {faq.answer}
                </Typography>
                </AccordionDetails>
            </Accordion>          
            )
        })}
    </div>
);
}