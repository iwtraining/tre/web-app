import {Button, TextField} from "@mui/material";
import "./styles.scss";

export default function AddUser() {
    const onFinish = (event: any) => {   
        console.log(event);        
        alert('Tudo ok');
    };   

    return (
        <section className="page-add-user">
            <h1>Novo Usuário</h1>   

            <form onSubmit={onFinish}>
                <TextField 
                    label="Nome"
                    name="Nome"
                    fullWidth
                />

                <TextField 
                    label="Email"
                    name="email"
                    fullWidth 
                />  

                <TextField 
                    label="Telefone"
                    name="telefone"
                    fullWidth 
                /> 

                <TextField 
                    label="Senha"
                    name="senha" 
                    fullWidth
                />  

                <Button onClick={onFinish} fullWidth color="primary" variant="contained">
                    ENVIAR
                </Button>     

            </form>    
        </section>
    );
} 