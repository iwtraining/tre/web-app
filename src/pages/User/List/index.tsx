import { useEffect, useState } from 'react';
import {Card, CardActionArea, CardContent, Divider, Typography} from '@mui/material';

export default function ListUsers() {
    interface User {
        name: string,
        email: string,
        phone: string
    };

    const [users, setUsers] = useState<User[]>([]);
    
    useEffect(() => {
        let url:string = process.env.REACT_APP_API_URL + '/users/';
        
        fetch(url)
          .then(response => response.json())
          .then(users => {
            setUsers(users);
          })
          .catch(error => {
            alert("Não foi possível ler o banco de dados!");
          });
   
    }, []);
    
    return (
        <section style={{width: '96%', margin: 'auto'}}>
            {users.map((user) => {
                return (
                    <Card style={{marginTop: 20}}>
                        <CardActionArea>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {user.name}
                                </Typography>

                                <Divider/>

                                <Typography gutterBottom component="p">
                                    {user.email}
                                </Typography>          
                                <Typography gutterBottom component="p">
                                    {user.phone}
                                </Typography>                         
                            </CardContent>
                        </CardActionArea>
                    </Card>
                )
            })}
        </section>
    )
}