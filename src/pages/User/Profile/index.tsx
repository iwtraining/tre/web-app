import {Button, TextField, Avatar} from "@mui/material";
import {Person as UserIcon} from "@mui/icons-material";
import "./styles.scss";

const onFinish = (event:any) => {            
    alert('Tudo ok');
};


export default function Profile() {
    return (
        <section className="page-profile">
            <h1>Meu Perfil</h1>

            <form onSubmit={onFinish}>
                <div style={{marginBottom: 30, textAlign: 'center', margin: 'auto', display: 'table'}}>
                    <Avatar sx={{width: 150, height: 150}}>
                        <UserIcon style={{fontSize: '8rem'}} />
                    </Avatar>
                </div>

                <TextField 
                    label="Nome"
                    name="Nome"
                    fullWidth
                />

                <TextField 
                    label="Email"
                    name="email"
                    fullWidth 
                />  

                <TextField 
                    label="Telefone"
                    name="telefone"
                    fullWidth 
                /> 

                <TextField 
                    label="Senha"
                    name="senha" 
                    type="password"
                    fullWidth
                />  


                <Button fullWidth type="submit" variant="contained">Entrar</Button>
            </form>        
        </section>
    );
}