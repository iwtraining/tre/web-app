import { useEffect, useState } from 'react';
import {Card, CardActionArea, CardContent, Divider, Typography} from '@mui/material';

export default function ListVehicles() {
    interface Vehicle {
        plate: string,
        make: string,
        model: string
    };

    const [vehicles, setVehicles] = useState<Vehicle[]>([]);
    
    useEffect(() => {
        let url:string = process.env.REACT_APP_API_URL + '/vehicles/';
        
        fetch(url)
          .then(response => response.json())
          .then(vehicles => {
            setVehicles(vehicles);
          })
          .catch(error => {
            alert("Não foi possível ler o banco de dados!");
          });
   
    }, []);
    
    return (
        <section style={{width: '96%', margin: 'auto'}}>
            {vehicles.map((vehicle) => {
                return (
                    <Card style={{marginTop: 20}}>
                        <CardActionArea>
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {vehicle.plate}
                                </Typography>

                                <Divider/>

                                <Typography gutterBottom component="p">
                                    {vehicle.make}
                                </Typography>
                                <Typography gutterBottom component="p">
                                    {vehicle.model}
                                </Typography>                                
                            </CardContent>
                        </CardActionArea>
                    </Card>
                )
            })}
        </section>
    )
}