import { useEffect, useState } from 'react';
import {Card, CardActionArea, CardContent, Divider, Typography} from '@mui/material';
import { Link } from 'react-router-dom';

export default function ListZones() {
    interface Zone {
        id: number,
        head_office: string,
        number: string,
    };

    const [zones, setZones] = useState<Zone[]>([]);
    
    useEffect(() => {
        let url:string = process.env.REACT_APP_API_URL + '/zones/';
        
        fetch(url)
          .then(response => response.json())
          .then(zones => {
            setZones(zones);
          })
          .catch(error => {
            alert("Não foi possível ler o banco de dados!");
          });
   
    }, []);
    
    return (
        <section style={{width: '96%', margin: 'auto'}}>
            {zones.map((zone) => {
                return (
                    <Link to={`/zonas/${zone.id}`}>
                        <Card style={{marginTop: 20}}>
                            <CardActionArea>
                                <CardContent>
                                    <Typography gutterBottom variant="h5" component="div">
                                        {zone.number}
                                    </Typography>

                                    <Divider/>

                                    <Typography gutterBottom component="p">
                                        {zone.head_office}
                                    </Typography>                                
                                </CardContent>
                            </CardActionArea>
                        </Card>
                    </Link>
                )
            })}
        </section>
    )
}