import { useEffect, useState } from 'react';
import {Card, CardActionArea, CardContent, Divider, Typography, Skeleton, LinearProgress} from '@mui/material';
import { useParams } from 'react-router';
import { RequestQuoteSharp } from '@mui/icons-material';

export default function Zone() {
    interface Request {
        id: string
    }

    interface Zone {
        id: number,
        head_office: string,
        number: string,
        judge: string,
        clerk: string,
        phone: string,
        email: string
    };

    const {id} = useParams<Request>();
    const [zone, setZone] = useState<Zone | null>(null);
    
    useEffect(() => {
        let url:string = process.env.REACT_APP_API_URL + `/zones/${id}`;
        
        setTimeout(() => {
            fetch(url)
            .then(response => response.json())
            .then(zone => {
              setZone(zone);
            })
            .catch(error => {
              alert("Não foi possível ler o banco de dados!");
            });
        }, 3000)
        
   
    }, []);

    const CardZone = () => {
        return (
            <Card style={{marginTop: 20}}>
                <CardActionArea>
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            Numero: {zone?.number}
                        </Typography>

                        <Divider/>

                        <Typography gutterBottom component="p">
                            Local: {zone?.head_office}
                        </Typography>
                        <Typography gutterBottom component="p">
                            Juiz: {zone?.judge}
                        </Typography>
                        <Typography gutterBottom component="p">
                            {zone?.clerk}
                        </Typography>  
                        <Typography gutterBottom component="p">
                            Telefone: {zone?.phone}
                        </Typography>  
                        <Typography gutterBottom component="p">
                            Email: {zone?.email}
                        </Typography>                            
                    </CardContent>
                </CardActionArea>
            </Card>
        );
    }

    const Loading = () => {
        return (
            <>
                <Skeleton variant="text"/>
                <Skeleton variant="circular" width={40} height={40} />
                <Skeleton variant="rectangular" width={210} height={118} />
            </>
        );
    }

    const LoadingProgress = () => {
        return (
            <>  
                <br/>
                <br/>
                <LinearProgress/>
            </>
        );
    }

    return (
        <section style={{width: '96%', margin: 'auto'}}>
            
            { !zone && <LoadingProgress/> }

            { zone && <CardZone/>}
        </section>
    )
}