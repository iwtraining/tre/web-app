const STORAGE_NAME = 'tre_user'

export function userIsLogged(): boolean {
    let user = localStorage.getItem(STORAGE_NAME);

    return user !== null; //true or false
}

export function login(user: any): any {
    localStorage.setItem(STORAGE_NAME, JSON.stringify(user));

    return true;
}

export function logout(): void {
    localStorage.removeItem(STORAGE_NAME);
}